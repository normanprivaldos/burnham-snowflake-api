# Burnham Snowflake API

This contains the overall build of the Burnham Snowflake API endpoint integrated using the NodeJS Driver, coupled with Express JS.

## Set-up
1. Create an `env` file, copy the content found inside `sample-env` and replace the following values with the correct credentials.
2. Apply the correct node version (`v10.13.0`), simply run the `nvm use` command to setup the correct node version.
3. To run the server you can choose either of the two commands:
	-	`npm run api`: This will not detect changes made in your code so you have to restart this command to apply your changes
	-	`npm run api-watch`: This will detect changes made in your code, once you save a file it will automatically restart the server to apply your changes.

## Building Models
For this project models are basically the tables found in the database, one model is equivalent to one table. We've built a boilerplate in order to initially load the data for each table:

1. In order to create a model, run the sh command: `sh model.sh <name_of_table_in_lowercase>`
	- For example: There's a table in the database called `PERMIT`, we can then run the command `sh model.sh permit`
2. You'll notice that it'll create a folder inside the `models` folder with 2 files in it: the core file (`named after the table`) and the config (`named after the table but with -config on the end`) file. Import the core file in the `server.js` file by simply adding this line of code:
	
	```
	const NameofModelInCamelCase = require('./models/<foldername>/<corefile>');
	```
	
	Example:
	
	```
	const Permit = require('./models/permit/permit');
	```
	
3. After importing the core file, visit the config file of the newly generated model. You'll noticed that inside the config file are 2 URL path: the `originUrl` and the `nextUrl`. Go back to the `server.js` and add the following URLs inside the `switch-case` statement.

	Example:
	
	```
	case 'permits': {
		break;
	}
	```
	
4. Visit the core file of the newly generated model. You'll noticed that inside the core file are 2 functions: the `get` function and the `getNext` function. Go bac k to the `server.js` and add the following function with the correct arguments to their corresponding case: the `get` function will go to the `originUrl` case and the `getNext` function will go to the `nextUrl` case.

	Example:
	
	```
	case 'permits': {
		Permit.get(snowflake, req, res);
		break;
	}
	```
	
5. That's it! You can now visit `http://localhost:3600/api/permits` and `http://localhost:3600/api/next-permits` in your browser.

## What's the Get and GetNext Function?
The `get` function is the main function of the model that communicates to the Snowflake Database, it gets the total number of the rows found on that table, caches the data found on that table, and returns only a chunk of it. The number of the data it'll return will be based on the `limit` found on the `global-config` file.

**What if I want to get ALL the data?** the get function returns the next URL you'll have to visit in order to get the next batch of data as well. Once it reached the end of the table, it'll no longer provide you with the next URL to visit.

The `getNext` function fetches a chunk of the cache data. This function will work only when the `get` function was called first, this is because the data the `getNext` function is looking up is based on the data cached by the `get` function.

## The Global Config File
Inside the `models` folder is a file called `global-config`. As of now, the only property inside that file is the result count limit being used by all the models. You can add other properties that you think will be used globally as well.