module.exports = {
  table: 'permit',
  originUrl: '/api/permits',
  nextUrl: '/api/next-permits',
  columns: [
    'PERMITID'
  ],
};
