const { table, columns, originUrl, nextUrl } = require('./permit-config');
const { limit } = require('../global-config');

let originalUrl = '';
let statement = null;
let totalRows = 0;

const get = async (snowflake, req, res) => {
  const data = [];
  const binds = [];

  originalUrl = req.originalUrl;

  let sqlText = `SELECT * FROM ${table}`;
  let next = `${nextUrl}?skip=${limit}&limit=${limit}`;

  const bounds = req.query.bounds;
  
  delete req.query.bounds;

  Object.keys(req.query).map(key => {
    let operator = Object.keys(req.query[key])[0];
    let operatorString = '';
    let value = req.query[key];

    if (operator) {
      value = req.query[key][operator];
      operatorString = `[${operator}]`;

      switch (operator) {
        case 'eq':
          operator = '=';
          break;
        case 'nq':
          operator = '!=';
          break;
        case 'lt':
          operator = '<';
          break;
        case 'lte':
          operator = '<=';
          break;
        case 'gt':
          operator = '>';
          break;
        case 'gte':
          operator = '>=';
          break;
        default:
          operator = '=';
          break;
      }
    } else {
      operator = '=';
    }

    if (columns.indexOf(key.toUpperCase()) >= 0) {
      sqlText += !sqlText.includes('WHERE')
        ? ` WHERE ${key.toUpperCase()} ${operator} :${(binds.length + 1)}`
        : ` AND ${key.toUpperCase()} ${operator} :${(binds.length + 1)}`;

      next += `&${key}${operatorString}=${value}`;
      binds.push(value);
    }
  });

  if (bounds && bounds.split(',').length === 4) {
    const points = bounds.split(',');
    const identifier = [`:${(binds.length + 1)}`, `:${(binds.length + 2)}`, `:${(binds.length + 3)}`, `:${(binds.length + 4)}`];

    sqlText += !sqlText.includes('WHERE')
      ? ` WHERE ((${identifier[0]} < ${identifier[2]} AND LATITUDE BETWEEN ${identifier[0]} AND ${identifier[2]}) OR (${identifier[0]} > ${identifier[2]} AND (LATITUDE BETWEEN ${identifier[0]} AND 180 OR LATITUDE BETWEEN -180 AND ${identifier[2]})))
          AND ((${identifier[1]} < ${identifier[3]} AND LONGITUDE BETWEEN ${identifier[1]} AND ${identifier[3]}) OR (${identifier[1]} > ${identifier[3]} AND (LONGITUDE BETWEEN ${identifier[1]} AND 180 OR LONGITUDE BETWEEN -180 AND ${identifier[3]})))`
      : ` AND ((${identifier[0]} < ${identifier[2]} AND LATITUDE BETWEEN ${identifier[0]} AND ${identifier[2]}) OR (${identifier[0]} > ${identifier[2]} AND (LATITUDE BETWEEN ${identifier[0]} AND 180 OR LATITUDE BETWEEN -180 AND ${identifier[2]})))
          AND ((${identifier[1]} < ${identifier[3]} AND LONGITUDE BETWEEN ${identifier[1]} AND ${identifier[3]}) OR (${identifier[1]} > ${identifier[3]} AND (LONGITUDE BETWEEN ${identifier[1]} AND 180 OR LONGITUDE BETWEEN -180 AND ${identifier[3]})))`;
    
    next += `&bounds=${bounds}`;

    binds.push(...points);
  }

  statement = snowflake.createStatement({ sqlText, binds, streamResult: true });

  try {
    await statement.execute();

    totalRows = statement.getNumRows();

    const meta = { total: totalRows };

    if (totalRows > limit) {
      meta['next'] = next;
    }

      statement.streamRows({ start: 0, end: totalRows > limit ? limit : totalRows - 1 })
        .on('error', () => {
          res.status(400).send({
            error: 'Stream Error'
          });
        })
        .on('data', row => data.push(row))
        .on('end', () => res.json({ meta, data }));
    } catch (err) {
      res.status(400).send({
        error: 'Incorrect query parameters'
      });
    }
 };

 const getNext = (req, res) => {
   let data = [];

   const skip = parseInt(req.query.skip || 0);
   const limit = parseInt(req.query.limit || limit);

   const start = skip + 1;
   const end = skip + limit;

   if (statement) {
     let next = `${nextUrl}?skip=${end}&limit=${limit}`;

     Object.keys(req.query).map(key => {
      if (columns.indexOf(key.toUpperCase()) >= 0) {
        const operator = Object.keys(req.query[key])[0];
        let value = req.query[key];
        let operatorString = '';

        if (operator) {
          operatorString = `[${operator}]`;
          value = req.query[key][operator]; 
        }
        next += `&${key}${operatorString}=${value}`;
      }
     });

     if (req.query.bounds) {
       next += `&bounds=${req.query.bounds}`;
     }

     const meta = { total: totalRows };

     if (totalRows > limit) {
       meta['next'] = next;
     }

     statement.streamRows({ start, end })
       .on('error', () => {
         res.status(400).send({
           error: 'Incorrect query parameters'
         });
       })
       .on('data', row => data.push(row))
       .on('end', () => res.json({ meta, data }));
   } else {
     res.status(404).send({
       error: `Origin endpoint not found. Try accessing the ${originUrl} first`,
     });
   }
 };

 module.exports = {
   get,
   getNext,
 };

