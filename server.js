'use-strict';

require('dotenv').config();

const Snowflake = require('snowflake-promise').Snowflake;
const app = require('express')();
const cors = require('cors');

app.use(cors());

const snowflake = new Snowflake({
  account: process.env.SNOWFLAKE_ACCOUNT,
  username: process.env.SNOWFLAKE_USERNAME,
  password: process.env.SNOWFLAKE_PASSWORD,
  database: process.env.SNOWFLAKE_DATABASE,
  warehouse: process.env.SNOWFLAKE_WAREHOUSE,
  schema: process.env.SNOWFLAKE_SCHEMA,
}, {
  logLevel: 'trace',
  logSql: console.log,
});

app.listen(3600, async () => await snowflake.connect());

const Permit = require('./models/permit/permit');

app.get('/api/:path', (req, res) => {
  if (req.params.path && req.params.path.trim().length > 0) {

    // specify the path of the new model created inside
    // the switch case statement
    switch (req.params.path) {
      case 'permits': {
        Permit.get(snowflake, req, res);
        break;
      }
      case 'next-permits': {
        Permit.getNext(req, res);
        break;
      }
    }
  }
});